# README #

Vue + Webpack + mocha example

### 環境需求 ? ###

* node v6 or later

### 如何安裝 ? ###

下載安裝：
```sh
$ git clone https://bbkings2002@bitbucket.org/bbkings2002/vue-webpack-mocha-example.git [your project]
$ cd [your folder]
$ npm install
```

相關指令：
```sh
$ npm run dev
$ npm run tdd
$ npm test
```

測試案例：

* 本測試使用 karma + mocha + phantomjs + sinon
* 測試案例檔案：[your project]/test/**.test.js
* 測試內容包含：
1. DOM render testing
2. two-way data binding testing
3. invoke method testing


參考資源：

* https://github.com/vuejs-templates/webpack
* https://mochajs.org/
* https://webpack.js.org/configuration/
* http://chaijs.com/
* http://sinonjs.org/releases/v2.1.0/